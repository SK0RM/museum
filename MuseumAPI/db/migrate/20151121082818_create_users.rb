class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login
      t.string :api_token

      t.timestamps null: false
    end
  end
end
