class ImageController < BaseAPIController
  before_action :find_image, :only => [:update, :remove]

  def index
    render json: Image.all
  end

  def create
    if Image.find_by_url(image_params['url'])
      render nothing: true, status: :unprocessable_entity
    else
      @image = Image.new(image_params)
      if @image.save
        render nothing: true, status: :ok
      else
        render nothing: true, status: :unprocessable_entity
      end
    end
  end

  def update
    if @image
      @image.update(image_params)
      if @image.save
        render nothing: true, status: :ok
      else
        render nothing: true, status: :unprocessable_entity
      end
    else
      render nothing: true, status: :unprocessable_entity
    end
  end

  def remove
    if @image && @image.destroy
      render nothing: true, status: :ok
    else
      render nothing: true, status: :unprocessable_entity
    end
  end

  private

  def image_params
    params.require(:image).permit(:name, :url, :url_base64)
  end

  def find_image
    @image = Image.find_by_url(Base64.decode64(params['url_base64']))
  end

end
