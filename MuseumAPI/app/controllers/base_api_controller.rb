class BaseAPIController < ApplicationController
  #before_filter :parse_request#, :authenticate_api_token # FIXME

  private

  def authenticate_api_token
    api_token = @json.has_key?('api_token') ? @json['api_token'] : nil
    if !api_token
      render nothing: true, status: :unauthorized
    else
      @user = nil
      User.find_each do |u|
        if Devise.secure_compare(u.api_token, api_token)
          @user = u
          break
        end
      end
    end
  end

  def parse_request
    request.body.rewind
    json = request.body.read
    puts 'json: ' + json
    @json = JSON.parse(json.length > 2 ? json : '{}')
  end
end